# FIRST CLOUD FUNCTION TEST 

- Http Trigger
- Not yet a module

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VPC | The ID of the VPC | String | - |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| Security group ID | ID of the group created |

# Usage
- URL/test-python-http?message=hello ric
  
