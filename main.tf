provider "google" {
 region      = "us-east1"
}

resource "google_cloudfunctions_function" "test" {
    name                      = "test-python-http"
    entry_point               = "hello_world"
    available_memory_mb       = 128
    timeout                   = 61
    project                   = var.project
    region                    = "us-east1"
    trigger_http              = true
    runtime                   = "python37"
    #trigger_topic             = "[PubSubTopic]"
    #trigger_bucket            = "[StorageBucketName]"
    source_archive_bucket     = "${google_storage_bucket.bucket.name}"
    source_archive_object     = "${google_storage_bucket_object.archive.name}"
    labels  = {
        deployment_name           = var.env
    }
}

resource "google_storage_bucket" "bucket" {
  name                      = "cloudfunction-test1"
  project                   = var.project
}

data "archive_file" "http_trigger" {
  type              = "zip"
  source_file       = "${path.module}/scripts/main.py"
  output_path       = "${path.module}/scripts/http_trigger.zip"
}

resource "google_storage_bucket_object" "archive" {
  name   = "http_trigger.zip"
  bucket = "${google_storage_bucket.bucket.name}"
  source = "${path.module}/scripts/http_trigger.zip"
  depends_on = ["data.archive_file.http_trigger"]
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.test.project
  region         = google_cloudfunctions_function.test.region
  cloud_function = google_cloudfunctions_function.test.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}