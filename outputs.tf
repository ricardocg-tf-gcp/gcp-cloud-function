output "http-url" {
    description = "URL which triggers function execution."
    value       = google_cloudfunctions_function.test.https_trigger_url
}