variable "env" {
    type        = "string"
    description = "Env for tags"
    default     = "test"
}

variable "project" {
    type        = "string"
    description = "Project ID"
    default     = "optimum-archery-284619"
}